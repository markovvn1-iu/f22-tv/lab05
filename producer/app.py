import os
import pika
import pika.channel
from pika.adapters.blocking_connection import BlockingChannel
from loguru import logger
import json
import random
import time


class AMQPProducer:
    QUEUE = "my_queue"  # Name of the RabbitMQ queue

    _connection: pika.BlockingConnection
    _channel: BlockingChannel

    def __init__(self, amqp_url: str) -> None:
        logger.debug("Connecting to {} (queue {})", amqp_url, self.QUEUE)
        self._connection = pika.BlockingConnection(pika.URLParameters(amqp_url))
        self._channel = self._connection.channel()
        self._channel.queue_declare(queue=self.QUEUE)

    def close(self) -> None:
        logger.debug("Closing connection")
        self._connection.close()

    def send_message(self, body: bytes) -> None:
        logger.debug("Sending message: {}", body)
        self._channel.basic_publish(exchange="", routing_key=self.QUEUE, body=body)


def main():
    amqp_url = os.environ["AMQP_URL"]
    producer = AMQPProducer(amqp_url)

    try:
        while True:
            if random.random() >= 0.5:
                msg = json.dumps(
                    {
                        "type": 0,
                        "some_useless_data": random.randint(0, 255),
                        "more_data": random.randint(0, 255),
                    }
                ).encode()
                producer.send_message(msg)
            else:
                msg = json.dumps(
                    {
                        "type": 1,
                        "my_name": ["olya", "kolya", "masha", "ita"][
                            random.randint(0, 3)
                        ],
                        "comment": "some comment",
                    }
                ).encode()
                producer.send_message(msg)

            time.sleep(5)
    except KeyboardInterrupt:
        producer.close()


if __name__ == "__main__":
    main()
