import os
from datetime import datetime, timezone
from typing import Any, ContextManager, Dict, List, Optional

from loguru import logger
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.orm import Session, sessionmaker


from model import Type1DB, Type2DB, SQLBase


class DatabaseRepsitory:
    _engine: Engine
    _make_session: sessionmaker

    def __init__(self, db_url: str) -> None:
        self._engine = create_engine(db_url)  # , echo=True
        self._make_session = sessionmaker(autocommit=False, autoflush=False, bind=self._engine)
        SQLBase.metadata.create_all(bind=self._engine)

    def _get_session(self) -> ContextManager[Session]:
        return self._make_session()

    def new_type1(self, some_useless_data: int, more_data: int) -> int:
        with self._get_session() as sess:
            analysis = Type1DB(
                some_useless_data=some_useless_data,
                more_data=more_data,
            )
            sess.add(analysis)
            sess.commit()

            logger.info(
                "New type1 (some_useless_data={}, more_data={}) was created with id={}",
                some_useless_data,
                more_data,
                analysis.type1_id,
            )
        return analysis.type1_id

    def new_type2(self, my_name: str, comment: str) -> int:
        with self._get_session() as sess:
            analysis = Type2DB(
                my_name=my_name,
                comment=comment,
            )
            sess.add(analysis)
            sess.commit()

            logger.info(
                "New type1 (my_name={}, comment={}) was created with id={}",
                my_name,
                comment,
                analysis.type2_id,
            )
        return analysis.type2_id
