import os
from typing import Dict, Any
import pika
import pika.channel
import pika.spec
from pika.adapters.blocking_connection import BlockingChannel
from loguru import logger
import json

from db import DatabaseRepsitory


class AMQPConsumer:
    QUEUE = "my_queue"  # Name of the RabbitMQ queue

    _connection: pika.BlockingConnection
    _channel: BlockingChannel

    _db: DatabaseRepsitory

    def __init__(self, amqp_url: str, db: DatabaseRepsitory) -> None:
        self._db = db

        logger.debug("Connecting to {} (queue {})", amqp_url, self.QUEUE)
        self._connection = pika.BlockingConnection(pika.URLParameters(amqp_url))
        self._channel = self._connection.channel()
        self._channel.queue_declare(queue=self.QUEUE)
        self._channel.basic_consume(
            queue=self.QUEUE, on_message_callback=self.callback, auto_ack=True
        )

    def start(self) -> None:
        logger.debug("Start consuming messages")
        self._channel.start_consuming()

    def close(self) -> None:
        logger.debug("Closing connection")
        self._connection.close()

    def callback(
        self,
        channel: BlockingChannel,
        method: pika.spec.Basic.Deliver,
        properties: pika.spec.BasicProperties,
        body: bytes,
    ):
        logger.debug("Recived message: {}", body)
        data: Dict[str, Any] = json.loads(body)
        if data.get("type") == 0:
            self._db.new_type1(data["some_useless_data"], data["more_data"])
        elif data.get("type") == 1:
            self._db.new_type2(data["my_name"], data["comment"])


def main():
    amqp_url = os.environ["AMQP_URL"]
    db_url = os.environ["DB_URL"]
    consumer = AMQPConsumer(amqp_url, DatabaseRepsitory(db_url))

    try:
        consumer.start()  # Run forever
    except KeyboardInterrupt:
        consumer.close()


if __name__ == "__main__":
    main()
