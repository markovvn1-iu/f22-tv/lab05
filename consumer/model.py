from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import functions

SQLBase = declarative_base()


class Type1DB(SQLBase):
    __tablename__ = "type1"
    type1_id = Column(Integer, primary_key=True, nullable=False, index=True)
    some_useless_data = Column(Integer, nullable=False)
    more_data = Column(Integer, nullable=False)
    created_at = Column(DateTime, server_default=functions.now(), index=True, nullable=False)


class Type2DB(SQLBase):
    __tablename__ = "type2"
    type2_id = Column(Integer, primary_key=True, nullable=False, index=True)
    my_name = Column(String(20), nullable=False)
    comment = Column(String, nullable=False)
    created_at = Column(DateTime, server_default=functions.now(), index=True, nullable=False)
