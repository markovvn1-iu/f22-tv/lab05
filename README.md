<div align="center">
  <img src=".gitlab/logo.svg" height="80px"/><br/>
  <h1>Lab 5</h1>
  <p>Docker & Kubernetes</p>
</div>

## 📝 Description

This project contains a solution to lab 5 in the Total Virtualisation course.

<img src=".gitlab/gitlab.svg" height="18px" /> Gitlab link: https://gitlab.com/markovvn1-iu/f22-tv/lab05

## Task 1. Dockerfile

1. Let's create a simplest dockerfile possible. It follow all best practices:

   ```dockerfile
   FROM alpine:3.17.0
   ```

2. Build and run it:

   ```bash
   docker build -t my_docker .
   docker run --rm -it my_docker
   apk add sysbench
   ```

3. Benchmark your Docker container:

   | Metric                 | Sysbench command                                             | Why this command                  | What is interesting in sysbench output          |
   | ---------------------- | ------------------------------------------------------------ | --------------------------------- | ----------------------------------------------- |
   | CPU performance        | sysbench --time=60 cpu --cpu-max-prime=64000 run             | Because this command was in lab 3 | Events per second, Latency avg                  |
   | CPU thread performance | sysbench --threads=10 --time=60 cpu --cpu-max-prime=64000 run | Because this command was in lab 3 | Events per second, Latency avg                  |
   | Scheduler performance  | sysbench --num-threads=64 --test=threads --thread-yields=100 --thread-locks=2 run | Because this command was in lab 3 | Total time, Total number of events, Latency avg |
   | Thread memory access   | sysbench --threads=10 --time=60 memory --memory-oper=write run | Because this command was in lab 3 | Total time, Latency max                         |
   | Memory access          | sysbench --test=memory --memory-block-size=1M --memory-total-size=10G run | Because this command was in lab 3 | Total time, Latency max                         |
   | File I/O read/write    | sysbench --test=fileio --file-total-size=512M --file-test-mode=rndrw  --time=120 --max-time=300 --max-requests=0 run | Because this command was in lab 3 | File operations, Throughput                     |

   Result of measurements:

   |                                                | Host machine | Multipass VM | LXC     | Docker     |
   | ---------------------------------------------- | ------------ | ------------ | ------- | ---------- |
   | CPU performance (Events per second)            | **122.45**   | 116.78       | 119.74  | 108.52     |
   | CPU performance (Latency avg)                  | **8.17**     | 8.56         | 8.35    | 9.21       |
   | CPU thread performance (Events per second)     | **614.38**   | 402.021      | 609.61  | 558.35     |
   | CPU thread performance (Latency avg)           | **16.27**    | 24.912       | 16.40   | 17.90      |
   | Scheduler performance (Total time, s)          | 10.0034      | 10.4118      | 10.0042 | 10.0103    |
   | Scheduler performance (Total number of events) | **139325**   | 878          | 133917  | 69006      |
   | Scheduler performance (Latency avg)            | **4.59**     | 834.61       | 4.78    | 9.28       |
   | Thread memory access (Total time)              | 7.2525       | 9.6059       | 7.6046  | **6.9197** |
   | Thread memory access (Latency max)             | 32.52        | 37.37        | 33.65   | **32.04**  |
   | Memory access (Total time)                     | **0.3976**   | 0.4754       | 0.4139  | 0.6435     |
   | Memory access (Latency max)                    | **0.17**     | 1.23         | 2.03    | 0.99       |
   | File I/O (File operations, reads/s)            | 177.42       | **261.21**   | 175.22  | 177.85     |
   | File I/O (File operations, writes/s)           | 118.28       | **174.14**   | 116.81  | 118.56     |
   | File I/O (File operations, fsyncs/s)           | 378.52       | **557.25**   | 373.93  | 379.74     |
   | File I/O (Throughput read, MiB/s)              | 2.77         | **4.08**     | 2.74    | 2.78       |
   | File I/O (Throughput written, MiB/s)           | 1.85         | **2.72**     | 1.83    | 1.85       |

4. It seems like LXC in general is better than Docker. Probably because LXC is better integrated with Linux. Docker also uses the Docker Engine layer to work, which makes it more portable, while LXC communicates directly with the Linux kernel.

## Task 2. Dockerize an application

1. Use this resource to create python app for RabbitMQ: [link](https://www.rabbitmq.com/tutorials/tutorial-one-python.html)

2. My application randomly generate and send message of one of two types:

   ![](.gitlab/img04.png)

3. I will use previously created docker file for the python app

4. Passing variables at this step doesn't make any sense. I will do it in the task 3

5. ```bash
   git clone https://gitlab.com/markovvn1-iu/f22-tv/lab05.git && cd lab05
   cd consumer
   docker build -t markovvn1/iu-tv:lab05_consumer .
   docker push markovvn1/iu-tv:lab05_consumer
   ```

## Task 3. Docker compose

See `docker-compose.yaml` for the result of my work. It has my previously dockerized app and my OWN rabbitmq. Resources for my apps are limited.

## Task 4. Docker Orchestration

1. Write one more python applications (I am using SQLAlchemy to access PostgreSQL database). This application listens for messages and, depending on the type of message, creates an entry in one of two tables in the database:

   ![](.gitlab/img05.png)

2. Update docker-compose.yaml with new applications. See `docker-compose.yaml` for the results of my work.

3. Convert docker-compose.yaml to config for k8s ([link](https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/)):

   ```bash
   kompose convert -o kubernetes.yaml
   ```

4. Apply deployment:

   ```bash
   kubectl apply -f kubernetes.yaml
   ```

5. Get list of pods:

   ![](.gitlab/img01.png)

6. Expose all services:

   <img src=".gitlab/img02.png" style="zoom: 67%;" />

7. Open the database and see the results:

   ![](.gitlab/img03.png)

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>